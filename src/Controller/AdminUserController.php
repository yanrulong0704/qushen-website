<?php

namespace App\Controller;

use App\Entity\SysUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminUserController extends AbstractController
{
    /**
     * 用户列表
     * @Route("/admin/user_list", name="admin_list")
     */
    public function index(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $str = '';
                $page = $request->get('page') ?: 1;
                $rows = $request->get('rows') ?: 20;
                if (!empty($request->get('text'))) $str .= 'WHERE msg.title LIKE \'%' . $request->get('text') . '%\'';
                $dql = 'select msg.id,msg.username,msg.email,msg.phone,msg.title,msg.role,msg.state from App:SysUser msg ' . $str;
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $data = $query->execute();
                $sum = count($data);
                $pageCount = ceil($sum / $rows);
                if ($page > $pageCount) {
                    $page = $pageCount;
                }
                if ($rows > $sum) {
                    $rows = $sum;
                }
                $tabledata = $query->setFirstResult(($page - 1) * $rows)->setMaxResults($rows)->execute();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => $tabledata]);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * 添加用户
     * @Route("/admin/user_add", name="admin_user_add")
     */
    public function add(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'POST') {
                $user = new SysUser();
                $user->setUsername($request->get('username'));
                $user->setPassword($request->get('password'));
                $user->setCreatetime(new \DateTime());
                $user->setTitle($request->get('title'));
                $user->setState($request->get('state'));
                $user->setEmail($request->get('email'));
                $user->setPhone($request->get('phone'));
                $query = $this->get('doctrine')->getManager();
                $query->persist($user);
                $query->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '添加成功!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }
    /**
     * 修改用户信息
     * @Route("/admin/user_edit", name="admin_user_edit")
     */
    public function edit(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $dql = 'select msg.id,msg.username,msg.email,msg.phone,msg.title,msg.role,msg.state from App:SysUser msg where msg.id = :id';
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $tabledata = $query->setParameters(['id' => $request->get('id')])->execute();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => $tabledata]);
            } else if ($request->getMethod() == 'POST') {
                $user = $this->get('doctrine')->getManager()->find('App:SysUser', $request->get('id'));
                //检验用户名是否重复
                if ($this->check_name($request->get('username')) == 1) return new JsonResponse(['state' => 'error', 'is_session' => true, 'msg' => '已存在的用户名!']);
                $user->setUsername($request->get('username'));
                //检验邮箱是否重复
                if ($this->check_email($request->get('email')) == 1) return new JsonResponse(['state' => 'error', 'is_session' => true, 'msg' => '已存在的电子邮箱!']);
                $user->setEmail($request->get('email'));
                //检验手机号是否重复
                if ($this->check_phone($request->get('phone')) == 1) return new JsonResponse(['state' => 'error', 'is_session' => true, 'msg' => '已存在的手机号!']);
                $user->setPhone($request->get('phone'));

                $user->setPassword($request->get('password'));
                $user->setCreatetime(new \DateTime());
                $user->setTitle($request->get('title'));
                $user->setState($request->get('state'));
                $query = $this->get('doctrine')->getManager();
                $query->persist($user);
                $query->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '修改成功!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * 启用状态
     * @Route("/admin/user_is_show", name="admin_user_is_show")
     */
    public function is_show(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            $query = $this->get('doctrine')->getManager();
            $bus = $query->find('App:SysUser', $request->get('id'));
            $bus->setState($request->get('state'));
            $query->persist($bus);
            $query->flush();
            return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '修改成功!']);
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * 友情链接删除
     * @Route("/admin/user_del", name="admin_user_del")
     */
    public function del(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            foreach ($request->get('ids') as $id) {
                $bus = $this->get('doctrine')->getManager()->find('App:SysUser', $id);
                $this->get('doctrine')->getManager()->remove($bus);
            }
            $this->get('doctrine')->getManager()->flush();
            return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '删除成功!']);
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }





    //校验用户名是否存在
    private function check_name($name)
    {
        $dql = 'select msg.id from App:SysUser msg where msg.username = :name';
        $query = $this->get('doctrine')->getManager()->createQuery($dql);
        $data = $query->setParameters(['name' => $name])->execute();
        if (count($data) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    //校验邮箱是否存在
    private function check_email($email)
    {
        $dql = 'select msg.id from App:SysUser msg where msg.email = :email';
        $query = $this->get('doctrine')->getManager()->createQuery($dql);
        $data = $query->setParameters(['email' => $email])->execute();
        if (count($data) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    //校验手机号是否存在
    private function check_phone($phone)
    {
        $dql = 'select msg.id from App:SysUser msg where msg.phone = :phone';
        $query = $this->get('doctrine')->getManager()->createQuery($dql);
        $data = $query->setParameters(['phone' => $phone])->execute();
        if (count($data) > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}

<?php

namespace App\Controller;

use App\Entity\JobClassify;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminClassifyController extends AbstractController
{
    private $arrs = [];
    private $i = -1;

    /**
     * 分类列表
     * @Route("/admin/classify_list", name="admin_classify_list")
     */
    public function data(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $str = '';
                if ($request->get('text')) $str .= ' where info.title LIKE \'%' . $request->get('text') . '%\' ';
                $dql = 'select info.id,info.title,info.cut,info.pid from App:JobClassify info ' . $str;
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $tabledata = $query->execute();
                $data = [];
                $packData = [];
                foreach ($tabledata as $val) {
                    $packData[$val['id']] = $val;
                }
                foreach ($packData as $key => $val) {
                    if ($val['pid'] == 0) {
                        $data[] = &$packData[$key];
                    } else {
                        $packData[$val['pid']]['list'][] = &$packData[$key];
                    }
                }
                $this->list($data);
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => $this->arrs]);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * 分类添加
     * @Route("/admin/classify_add", name="admin_classify_add")
     */
    public function add(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $str = '';
                if ($request->get('text')) $str .= ' where info.title LIKE \'%' . $request->get('text') . '%\' ';
                $dql = 'select info.id,info.title,info.cut,info.pid from App:JobClassify info ' . $str;
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $tabledata = $query->execute();
                $data = [];
                $packData = [];
                foreach ($tabledata as $val) {
                    $packData[$val['id']] = $val;
                }
                foreach ($packData as $key => $val) {
                    if ($val['pid'] == 0) {
                        $data[] = &$packData[$key];
                    } else {
                        $packData[$val['pid']]['list'][] = &$packData[$key];
                    }
                }
                $this->list($data);
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => $this->arrs]);

            } else if ($request->getMethod() == 'POST') {
                $classify = new JobClassify();
                $classify->setPid($request->get('pid'));
                $classify->setCut($request->get('cut'));
                $classify->setIdent($request->get('ident'));
                $classify->setTitle($request->get('title'));
                $classify->setInfo($request->get('info'));
                $classify->setSeotitle($request->get('seotitle'));
                $classify->setSeodescribe($request->get('seodescribe'));
                $classify->setSeokey($request->get('seokey'));
                $classify->setAddtime(new \DateTime());
                $query = $this->get('doctrine')->getManager();
                $query->persist($classify)->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '数据添加成功!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * 分类修改
     * @Route("/admin/classify_edit", name="admin_classify_edit")
     */
    public function edit(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $dql = 'select info.id,info.title,info.cut,info.pid from App:JobClassify info ';
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $tabledata = $query->execute();
                $data = [];
                $packData = [];
                foreach ($tabledata as $val) {
                    $packData[$val['id']] = $val;
                }
                foreach ($packData as $key => $val) {
                    if ($val['pid'] == 0) {
                        $data[] = &$packData[$key];
                    } else {
                        $packData[$val['pid']]['list'][] = &$packData[$key];
                    }
                }
                $this->list($data);
                $dql = 'select info1.id,info1.title,info1.cut,info1.pid,info1.info,info1.title,info1.ident,info1.seotitle,info1.seodescribe,info1.seokey from App:JobClassify info1 where info1.id = :id';
                $data = $this->get('doctrine')->getManager()->createQuery($dql)->setParameters(['id' => $request->get('id')])->execute();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => [
                    'classifg' => $this->arrs,
                    'data' => $data
                ]]);

            } else if ($request->getMethod() == 'POST') {
                $classify = $this->get('doctrine')->getManager()->find('App:JobClassify', $request->get('id'));
                $classify->setPid($request->get('pid'));
                $classify->setCut($request->get('cut'));
                $classify->setIdent($request->get('ident'));
                $classify->setTitle($request->get('title'));
                $classify->setInfo($request->get('info'));
                if (!empty($request->get('seotitle'))) $classify->setSeotitle($request->get('seotitle'));
                if (!empty($request->get('seodescribe'))) $classify->setSeodescribe($request->get('seodescribe'));
                if (!empty($request->get('seokey'))) $classify->setSeokey($request->get('seokey'));
                $classify->setAddtime(new \DateTime());
                $query = $this->get('doctrine')->getManager();
                $query->persist($classify)->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '数据修改成功!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * 分类删除
     * @Route("/admin/classify_del", name="admin_classify_del")
     */
    public function del(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            foreach ($request->get('ids') as $id) {
                $bus = $this->get('doctrine')->getManager()->find('App:JobClassify', $id);
                $this->get('doctrine')->getManager()->remove($bus);
            }
            $this->get('doctrine')->getManager()->flush();
            return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '删除成功!']);
        }else{
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    private function list(array $arr)
    {
        $this->i++;
        foreach ($arr as $key => $item) {
            if (isset($arr[$key]['list'])) {
                unset($item['list']);
                $item['title'] = str_repeat('---', $this->i) . $item['title'];
                array_push($this->arrs, $item);
                $this->list($arr[$key]['list']);
            } else {
                $item['title'] = str_repeat('---', $this->i) . $item['title'];
                array_push($this->arrs, $item);
            }
        }
        $this->i--;
    }
}

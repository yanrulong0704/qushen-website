<?php

namespace App\Controller;

use App\Entity\JobLink;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminLinkController extends AbstractController
{
    /**
     * 获取友情链接列表
     * @Route("/admin/link_list", name="admin_link_list")
     */
    public function index(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $str = '';
                $page = $request->get('page') ?: 1;
                $rows = $request->get('rows') ?: 20;
                if (!empty($request->get('text'))) $str .= 'WHERE msg.title LIKE \'%' . $request->get('text') . '%\'';
                $dql = 'select msg.id,msg.title,msg.img,msg.link,msg.state,msg.info,msg.addtime from App:JobLink msg ' . $str;
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $data = $query->execute();
                $sum = count($data);
                $pageCount = ceil($sum / $rows);
                if ($page > $pageCount) {
                    $page = $pageCount;
                }
                if ($rows > $sum) {
                    $rows = $sum;
                }
                $tabledata = $query->setFirstResult(($page - 1) * $rows)->setMaxResults($rows)->execute();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => $tabledata]);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }
    /**
     * 添加留言
     * @Route("/admin/link_add", name="admin_link_add")
     */
    public function add(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'POST') {
                $message = new JobLink();
                $message->setAddtime(new \DateTime());
                $message->setTitle($request->get('title'));
                $message->setImg($request->get('img'));
                $message->setInfo($request->get('info'));
                $message->setState($request->get('state'));
                $message->setLink($request->get('link'));
                $query = $this->get('doctrine')->getManager();
                $query->persist($message);
                $query->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '添加成功!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }
    /**
     * 修改友情链接
     * @Route("/admin/link_edit", name="admin_link_edit")
     */
    public function edit(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $dql = 'select msg.id,msg.title,msg.img,msg.link,msg.state,msg.info,msg.addtime  from App:JobLink msg where msg.id = :id';
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $tabledata = $query->setParameters(['id' => $request->get('id')])->execute();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => $tabledata]);
            } else if ($request->getMethod() == 'POST') {
                $link = $this->get('doctrine')->getManager()->find('App:JobLink', $request->get('id'));
                $link->setTitle($request->get('title'));
                $link->setImg($request->get('img'));
                $link->setInfo($request->get('info'));
                $link->setState($request->get('state'));
                $link->setLink($request->get('link'));
                $query = $this->get('doctrine')->getManager();
                $query->persist($link);
                $query->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '修改成功!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * 友情链接删除
     * @Route("/admin/link_del", name="admin_link_del")
     */
    public function del(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            foreach ($request->get('ids') as $id) {
                $bus = $this->get('doctrine')->getManager()->find('App:JobLink', $id);
                $this->get('doctrine')->getManager()->remove($bus);
            }
            $this->get('doctrine')->getManager()->flush();
            return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '删除成功!']);
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }
}

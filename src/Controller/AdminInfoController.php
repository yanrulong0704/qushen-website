<?php

namespace App\Controller;

use App\Entity\CompanyInfo;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class AdminInfoController extends AbstractController
{
    /**
     * 获取公司信息列表
     * @Route("/admin/info_list", name="admin_info_list")
     */
    public function index(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $str = '';
                $page = $request->get('page') ?: 1;
                $rows = $request->get('rows') ?: 20;
                if (!empty($request->get('text'))) $str .= 'WHERE msg.title LIKE \'%' . $request->get('text') . '%\'';
                $dql = 'select msg.id,msg.title,msg.info,msg.phone,msg.phone_2,msg.phone_3,msg.email,msg.address,msg.language from App:CompanyInfo msg ' . $str;
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $data = $query->execute();
                $sum = count($data);
                $pageCount = ceil($sum / $rows);
                if ($page > $pageCount) {
                    $page = $pageCount;
                }
                if ($rows > $sum) {
                    $rows = $sum;
                }
                $tabledata = $query->setFirstResult(($page - 1) * $rows)->setMaxResults($rows)->execute();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => $tabledata]);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }
    /**
     * 添加留言
     * @Route("/admin/info_add", name="admin_info_add")
     */
    public function add(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'POST') {
                $message = new CompanyInfo();
                $message->setTitle($request->get('title'));
                $message->setPhone($request->get('phone'));
                $message->setPhone2($request->get('phone2'));
                $message->setPhone3($request->get('phone3'));
                $message->setInfo($request->get('info'));
                $message->setLanguage($request->get('language'));
                $message->setEmail($request->get('email'));
                $message->setAddress($request->get('address'));
                $query = $this->get('doctrine')->getManager();
                $query->persist($message);
                $query->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '添加成功!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }
    /**
     * 修改公司信息
     * @Route("/admin/info_edit", name="admin_info_edit")
     */
    public function edit(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $dql = 'select msg.id,msg.title,msg.info,msg.phone,msg.phone_2,msg.phone_3,msg.email,msg.address,msg.language from App:CompanyInfo msg where msg.id = :id';
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $tabledata = $query->setParameters(['id' => $request->get('id')])->execute();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => $tabledata]);
            } else if ($request->getMethod() == 'POST') {
                $info = $this->get('doctrine')->getManager()->find('App:CompanyInfo', $request->get('id'));
                $info->setTitle($request->get('title'));
                $info->setPhone($request->get('phone'));
                $info->setPhone2($request->get('phone2'));
                $info->setPhone3($request->get('phone3'));
                $info->setInfo($request->get('info'));
                $info->setLanguage($request->get('language'));
                $info->setEmail($request->get('email'));
                $info->setAddress($request->get('address'));
                $query = $this->get('doctrine')->getManager();
                $query->persist($info);
                $query->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '修改成功!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * 友情链接删除
     * @Route("/admin/info_del", name="admin_info_del")
     */
    public function del(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            foreach ($request->get('ids') as $id) {
                $bus = $this->get('doctrine')->getManager()->find('App:CompanyInfo', $id);
                $this->get('doctrine')->getManager()->remove($bus);
            }
            $this->get('doctrine')->getManager()->flush();
            return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '删除成功!']);
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

}

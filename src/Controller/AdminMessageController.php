<?php

namespace App\Controller;

use App\Entity\JobMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminMessageController extends AbstractController
{
    /**
     * @Route("/admin/message_list", name="admin_message_list")
     */
    public function index(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $str = '';
                $page = $request->get('page') ?: 1;
                $rows = $request->get('rows') ?: 20;
                if (!empty($request->get('text'))) $str .= 'WHERE msg.phone LIKE \'%' . $request->get('text') . '%\'';
                $dql = 'select msg.id,msg.phone,msg.email,msg.title,msg.content,msg.addtime from App:JobMessage msg ' . $str;
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $data = $query->execute();
                $sum = count($data);
                $pageCount = ceil($sum / $rows);
                if ($page > $pageCount) {
                    $page = $pageCount;
                }
                if ($rows > $sum) {
                    $rows = $sum;
                }
                $tabledata = $query->setFirstResult(($page - 1) * $rows)->setMaxResults($rows)->execute();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => $tabledata]);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }
    /**
     * 添加留言
     * @Route("/admin/message_add", name="admin_message_add")
     */
    public function add(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'POST') {
                $message = new JobMessage();
                $message->setAddtime(new \DateTime());
                $message->setTitle($request->get('title'));
                $message->setPhone($request->get('phone'));
                $message->setEmail($request->get('email'));
                $message->setContent($request->get('content'));
                $query = $this->get('doctrine')->getManager();
                $query->persist($message);
                $query->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '添加成功!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }


    /**
     * 留言删除
     * @Route("/admin/message_del", name="admin_message_del")
     */
    public function del(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            foreach ($request->get('ids') as $id) {
                $bus = $this->get('doctrine')->getManager()->find('App:JobMessage', $id);
                $this->get('doctrine')->getManager()->remove($bus);
            }
            $this->get('doctrine')->getManager()->flush();
            return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '删除成功!']);
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }
}

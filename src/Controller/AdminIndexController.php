<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminIndexController extends AbstractController
{
    /**
     * @Route("/admin/index", name="admin_index")
     */
    public function index(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $articles = $this->getArticles();
                $message = $this->getMessage();
                $comment = $this->getComment();
                $pv = $this->getPv();
                $dayarr = [];
                $day = 30;
                if ($day != null) {
                    for ($i = -$day; $i <= 0; $i++) {
                        array_push($dayarr, date("Y年m月d日", strtotime($i . " day")));
                    }
                }
                $countdata = $this->getDayCount($day);
                $pv_30 = $this->get30Pv($countdata);
                $data = ['articles' => $articles, 'message' => $message, 'comment' => $comment, 'pv' => $pv, 'pv_30' => $pv_30, 'countdata' => $countdata];

                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => $data]);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }


    }
    //获取日期
    private function getDayCount($day)
    {
        $dayarr = [];
        for ($i = -$day; $i <= 0; $i++) {
            array_push($dayarr, date("Y-m-d", strtotime($i . " day")));
        }
        return $dayarr;
    }

    //获取文章数量
    private function getArticles()
    {
        $dql = "SELECT info.id FROM App:JobArticle info  ";
        $query = $this->get('doctrine')->getManager()->createQuery($dql);
        $sum = count($query->execute());
        return $sum;
    }

    //获取访问数量
    private function getPv()
    {
        $dql = "SELECT info.views FROM App:JobPv info  ";
        $query = $this->get('doctrine')->getManager()->createQuery($dql);
        $sum = $query->execute();
        $pv_con = 0;
        for ($i = 0; $i < count($sum); $i++) {
            $pv_con = $sum[$i]['views'] + $pv_con;
        }
        return $pv_con;
    }

    //获取访问数量
    private function get30Pv($day_arr)
    {
        $dql = "SELECT info.views,info.addtime FROM App:JobPv info  ORDER BY info.addtime DESC";
        $query = $this->get('doctrine')->getManager()->createQuery($dql);
        $sum = $query->execute();
        $pv_arr = [];
        for ($i = 0; $i < count($day_arr); $i++) {
            array_push($pv_arr, 0);
        }
        for ($i = 0; $i < count($day_arr); $i++) {
            for ($m = 0; $m < count($sum); $m++) {
                if ($sum[$m]['addtime'] == $day_arr[$i]) {
                    $pv_arr[$i] = $sum[$m]['views'];
                }
            }
        }
        return $pv_arr;
    }

    //获取留言
    private function getMessage()
    {
        $dql = "SELECT info.id FROM App:JobMessage info  ";
        $query = $this->get('doctrine')->getManager()->createQuery($dql);
        $sum = count($query->execute());
        return $sum;
    }

    //获取评论数量
    private function getComment()
    {
        $dql = "SELECT info.id FROM App:JobComment info  ";
        $query = $this->get('doctrine')->getManager()->createQuery($dql);
        $sum = count($query->execute());
        return $sum;
    }

}

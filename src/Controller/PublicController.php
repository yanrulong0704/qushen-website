<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PublicController extends AbstractController
{
    /**
     * @Route("/public/up", name="public_up")
     */
    public function index(Request $request)
    {
        if ($request->getMethod() == 'OPTIONS') {
            return new Response('', 201, array('Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Credentials' => 'true'));
        } else if ($request->getMethod() == 'POST') {
            try {
                $file = $request->files->get('upfile');
                $fileName = date('YmdHis_') . mt_rand(10000, 99999) . '.' . $file->guessExtension();
                $dir = $this->get('kernel')->getRootDir() . '/../public/assets/upload/file/';
                $file->move($dir, $fileName);
                return new Response('/upload/file/' . $fileName, 201, array('Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Credentials' => 'true'));
            } catch (\Exception $e) {
                $array = array('status' => 0);
                return new Response($array, 201, array('Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Credentials' => 'true'));
            }
        } else {
            return new Response('error_no_post', 201, array('Access-Control-Allow-Origin' => '*', 'Access-Control-Allow-Credentials' => 'true'));
        }
    }
}

<?php

namespace App\Controller;

use App\Entity\JobArticle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminArticleController extends AbstractController
{

    private $i = -1;
    private $arrs = [];

    /**
     * 文章列表
     * @Route("/admin/article_list", name="admin_article_list")
     */
    public function index(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $str = '';
                if (!empty($request->get('text'))) $str .= 'WHERE info.title LIKE \'%' . $request->get('text') . '%\'';
                $page = $request->get('page') ?: 1;
                $rows = $request->get('rows') ?: 4;
                $dql = "SELECT info.id,info.title,info.addtime,info.cid,info.author,info.intro,info.isshow,info.views,fl.title FROM App:JobArticle info LEFT JOIN App:JobClassify fl WITH fl.id = info.cid " . $str . " ORDER BY info.id DESC ";
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $data = $query->execute();
                $sum = count($data);
                $pageCount = ceil($sum / $rows);
                if ($page > $pageCount) {
                    $page = $pageCount;
                }
                if ($rows > $sum) {
                    $rows = $sum;
                }
                $tabledata = $query->setFirstResult(($page - 1) * $rows)->setMaxResults($rows)->execute();
            }
            return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取列表成功!', 'data' => $tabledata, 'info' => [
                'page' => $page,
                'rows' => $rows,
                'text' => $request->get('text')
            ]]);
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * 添加文章
     * @Route("/admin/article_add", name="admin_article_add")
     */
    public function add(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {
                $str = '';
                if ($request->get('text')) $str .= ' where info.title LIKE \'%' . $request->get('text') . '%\' ';
                $dql = 'select info.id,info.title,info.pid from App:JobClassify info ' . $str;
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $tabledata = $query->execute();
                $data = [];
                $packData = [];
                foreach ($tabledata as $val) {
                    $packData[$val['id']] = $val;
                }
                foreach ($packData as $key => $val) {
                    if ($val['pid'] == 0) {
                        $data[] = &$packData[$key];
                    } else {
                        $packData[$val['pid']]['list'][] = &$packData[$key];
                    }
                }
                $this->list($data);
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => $this->arrs]);

            } else if ($request->getMethod() == 'POST') {
                $article = new JobArticle();
                $article->setAddtime(new \DateTime());
                $article->setTitle($request->get('title'));
                $article->setCid($request->get('cid'));
                $article->setIntro($request->get('intro'));
                $article->setCfrom($request->get('cfrom'));
                $article->setContent($request->get('content'));
                $article->setImg($request->get('img'));
                $article->setAgentid($admin_id);
                $article->setAuthor($session->get('username'));
                $query = $this->get('doctrine')->getManager();
                $query->persist($article);
                $query->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '添加成功!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * 修改文章
     * @Route("/admin/article_edit", name="admin_article_edit")
     */
    public function edit(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            if ($request->getMethod() == 'GET') {

                $dql = 'select info.id,info.title,info.pid from App:JobClassify info ';
                $query = $this->get('doctrine')->getManager()->createQuery($dql);
                $tabledata = $query->execute();
                $data = [];
                $packData = [];
                foreach ($tabledata as $val) {
                    $packData[$val['id']] = $val;
                }
                foreach ($packData as $key => $val) {
                    if ($val['pid'] == 0) {
                        $data[] = &$packData[$key];
                    } else {
                        $packData[$val['pid']]['list'][] = &$packData[$key];
                    }
                }
                $this->list($data);
                $dql = 'select info.id,info.title,info.cid,info.intro,info.cfrom,info.content,info.img,info.agentid,info.author from App:JobArticle info where info.id = :id';
                $data = $this->get('doctrine')->getManager()->createQuery($dql)->setParameters(['id' => $request->get('id')])->execute();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '获取数据成功!', 'data' => [
                    'classifg' => $this->arrs,
                    'data' => $data
                ]]);

            } else if ($request->getMethod() == 'POST') {
                $article = $this->get('doctrine')->getManager()->find('App:JobArticle', $request->get('id'));
                $article->setAddtime(new \DateTime());
                $article->setTitle($request->get('title'));
                $article->setCid($request->get('cid'));
                $article->setIntro($request->get('intro'));
                $article->setCfrom($request->get('cfrom'));
                $article->setContent($request->get('content'));
                $article->setImg($request->get('img'));
                $article->setAgentid($admin_id);
                $article->setAuthor($session->get('username'));
                $query = $this->get('doctrine')->getManager();
                $query->persist($article);
                $query->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '修改成功!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * is_show
     * @Route("/admin/article_is_show", name="admin_article_is_show")
     */
    public function is_show(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            $query = $this->get('doctrine')->getManager();
            $bus = $query->find('App:JobArticle', $request->get('id'));
            $bus->setIsshow($request->get('is_show'));
            $query->persist($bus);
            $query->flush();
            return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '修改成功!']);
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    /**
     * 文章删除
     * @Route("/admin/article_del", name="admin_article_del")
     */
    public function del(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            foreach ($request->get('ids') as $id) {
                $bus = $this->get('doctrine')->getManager()->find('App:JobArticle', $id);
                $this->get('doctrine')->getManager()->remove($bus);
            }
            $this->get('doctrine')->getManager()->flush();
            return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '删除成功!']);
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未登录!']);
        }
    }

    private function list(array $arr)
    {
        $this->i++;
        foreach ($arr as $key => $item) {
            if (isset($arr[$key]['list'])) {
                unset($item['list']);
                $item['title'] = str_repeat('---', $this->i) . $item['title'];
                array_push($this->arrs, $item);
                $this->list($arr[$key]['list']);
            } else {
                $item['title'] = str_repeat('---', $this->i) . $item['title'];
                array_push($this->arrs, $item);
            }
        }
        $this->i--;
    }
}

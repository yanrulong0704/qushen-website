<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminAuthController extends AbstractController
{
    /**
     * 管理员登录
     * @Route("/admin/login", name="admin_login")
     */
    public function login(Request $request)
    {
        $session = $request->getSession();
        $dql = 'select info.id,info.username,info.password,info.title from App:SysUser info where info.username = :user and info.password = :pswd';
        $query = $this->get('doctrine')->getManager()->createQuery($dql);
        $data = $query->setParameters(['user' => $request->get('username'), 'pswd' => $request->get('password')])->execute();
        if (count($data) > 0) {
            $session->set('admin_id', $data[0]['id']);
            $session->set('username', $data[0]['username']);
            $session->set('title', $data[0]['title']);
            return new JsonResponse(['state' => 'win']);
        } else {
            return new JsonResponse(['state' => 'error']);
        }


    }

    /**
     * 注销登录
     * @Route("/admin/logout", name="admin_logout")
     */
    public function logout(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            $session = $request->getSession();
            $session->set('admin_id', null);
            $session->set('username', null);
            $session->set('title', null);
            return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '注销成功']);
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未知错误!请重新登录!']);
        }
    }

    /**
     * 重置密码
     * @Route("/admin/reset_password", name="admin_reset_password")
     */
    public function reset_password(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            $user = $this->get('doctrine')->getManager()->find('App:SysUser', $admin_id);
            $user->setPassword($request->get('password'));
            $this->get('doctrine')->getManager()->persist($user)->flush();
            return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '修改成功']);
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未知错误，请重新登录']);
        }
    }

    /**
     * 经销商密码修改
     * @Route("/admin/edit_password", name="admin_edit_password")
     */
    public function edit_password(Request $request)
    {
        $session = $request->getSession();
        $admin_id = $session->get('admin_id');
        if ($admin_id != null) {
            $info = $this->get('doctrine')->getManager()->find('App:SysUser', $admin_id);
            if ($info->getPassword() == $request->get('old_password')) {
                $info->setPassword('new_password');
                $this->get('doctrine')->getManager()->persist($info)->flush();
                return new JsonResponse(['state' => 'win', 'is_session' => true, 'msg' => '修改成功!']);
            } else {
                return new JsonResponse(['state' => 'error', 'is_session' => true, 'msg' => '初始密码错误!']);
            }
        } else {
            return new JsonResponse(['state' => 'error', 'is_session' => false, 'msg' => '未知错误!请重新登录!']);
        }
    }
}

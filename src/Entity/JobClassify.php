<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 14-8-12
 * Time: 下午8:21
 */
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * 分类表管理
 * @ORM\Entity
 * @ORM\Table(name="job_classify")
 */
class JobClassify {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer $id
     */
    public $id;
    /**
     *  上级分类
     *  @ORM\Column(type="integer",nullable=true)
     */
    public $pid;
    /**
     *  类型 cn 中文，en 英文
     *  @ORM\Column(type="string", length=16,nullable=true)
     */
    public $cut;
    /**
     *  分类标识
     *  @ORM\Column(type="string", length=128,nullable=true)
     */
    public $ident;
    /**
     *  分组名称
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $title;
    /**
     *  分组描述
     *  @ORM\Column(type="text",nullable=true)
     */
    public $info;
    /**
     *  添加时间
     *  @ORM\Column(type="datetime",nullable=true)
     */
    public $addtime;
    /**
     *  seo标题
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $seotitle;

    /**
     *  seo关键字
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $seokey;

    /**
     *  seo描述
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $seodescribe;

    /**
     * @return mixed
     */
    public function getIdent()
    {
        return $this->ident;
    }

    /**
     * @param mixed $ident
     */
    public function setIdent($ident)
    {
        $this->ident = $ident;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param mixed $pid
     */
    public function setPid($pid)
    {
        $this->pid = $pid;
    }

    /**
     * @return mixed
     */
    public function getCut()
    {
        return $this->cut;
    }

    /**
     * @param mixed $cut
     */
    public function setCut($cut)
    {
        $this->cut = $cut;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getAddtime()
    {
        return $this->addtime;
    }

    /**
     * @param mixed $addtime
     */
    public function setAddtime($addtime)
    {
        $this->addtime = $addtime;
    }

    /**
     * @return mixed
     */
    public function getSeotitle()
    {
        return $this->seotitle;
    }

    /**
     * @param mixed $seotitle
     */
    public function setSeotitle($seotitle)
    {
        $this->seotitle = $seotitle;
    }

    /**
     * @return mixed
     */
    public function getSeokey()
    {
        return $this->seokey;
    }

    /**
     * @param mixed $seokey
     */
    public function setSeokey($seokey)
    {
        $this->seokey = $seokey;
    }

    /**
     * @return mixed
     */
    public function getSeodescribe()
    {
        return $this->seodescribe;
    }

    /**
     * @param mixed $seodescribe
     */
    public function setSeodescribe($seodescribe)
    {
        $this->seodescribe = $seodescribe;
    }

}
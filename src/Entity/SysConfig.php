<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 14-8-12
 * Time: 下午8:21
 */
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * 系统管理管理
 * @ORM\Entity
 * @ORM\Table(name="sys_config")
 */
class SysConfig {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer $id
     */
    public $id;
    /**
     *  网站名称
     *  @ORM\Column(type="string", length=127,nullable=true)
     */
    public $title;
    /**
     *  网站类型
     *  @ORM\Column(type="string", length=64,nullable=true)
     */
    public $language;
    /**
     *  seo标题
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $seotitle;

    /**
     *  seo关键字
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $seokey;

    /**
     *  seo描述
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $seodescribe;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getSeotitle()
    {
        return $this->seotitle;
    }

    /**
     * @param mixed $seotitle
     */
    public function setSeotitle($seotitle)
    {
        $this->seotitle = $seotitle;
    }

    /**
     * @return mixed
     */
    public function getSeokey()
    {
        return $this->seokey;
    }

    /**
     * @param mixed $seokey
     */
    public function setSeokey($seokey)
    {
        $this->seokey = $seokey;
    }

    /**
     * @return mixed
     */
    public function getSeodescribe()
    {
        return $this->seodescribe;
    }

    /**
     * @param mixed $seodescribe
     */
    public function setSeodescribe($seodescribe)
    {
        $this->seodescribe = $seodescribe;
    }

}
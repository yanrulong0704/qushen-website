<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 14-8-12
 * Time: 下午8:21
 */
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * 幻灯片表管理
 * @ORM\Entity
 * @ORM\Table(name="job_slide")
 */
class JobSlide{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer $id
     */
    public $id;
    /**
     *  所在分类
     *  @ORM\Column(type="integer",nullable=true)
     */
    public $cid;
    /**
     *  幻灯片名称
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $title;
    /**
     *  幻灯片地址
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $url;
    /**
     *  幻灯片图片
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $img;
    /**
     *  描述
     *  @ORM\Column(type="text",nullable=true)
     */
    public $info;
    /**
     *  内容
     *  @ORM\Column(type="text",nullable=true)
     */
    public $content;
    /**
     *  添加时间
     *  @ORM\Column(type="datetime",nullable=true)
     */
    public $addtime;

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * @param mixed $cid
     */
    public function setCid($cid)
    {
        $this->cid = $cid;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getAddtime()
    {
        return $this->addtime;
    }

    /**
     * @param mixed $addtime
     */
    public function setAddtime($addtime)
    {
        $this->addtime = $addtime;
    }


}
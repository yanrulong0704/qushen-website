<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 14-8-12
 * Time: 下午8:21
 */
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * 浏览量管理
 * @ORM\Entity
 * @ORM\Table(name="job_pv")
 */
class JobPv {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer $id
     */
    public $id;
    /**
     *  分组描述
     *  @ORM\Column(type="string",length=64,nullable=true)
     */
    public $views;
    /**
     *  添加时间
     *  @ORM\Column(type="string",length=64,nullable=true)
     */
    public $addtime;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param mixed $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    /**
     * @return mixed
     */
    public function getAddtime()
    {
        return $this->addtime;
    }

    /**
     * @param mixed $addtime
     */
    public function setAddtime($addtime)
    {
        $this->addtime = $addtime;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 14-8-12
 * Time: 下午8:21
 */
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * 用户表
 * @ORM\Entity
 * @ORM\Table(name="sys_user")
 */
class SysUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    public $id;
    /**
     *  用户名
     *  @ORM\Column(type="string", length=128,nullable=true)
     */
    public $username;
    /**
     *  电子邮箱
     *  @ORM\Column(type="string", length=128,nullable=true)
     */
    public $email;
    /**
     *  电话号码
     *  @ORM\Column(type="string", length=16,nullable=true)
     */
    public $phone;
    /**
     *  昵称
     *  @ORM\Column(type="string", length=128,nullable=true)
     */
    public $title;
    /**
     *  密码
     *  @ORM\Column(type="string", length=128,nullable=true)
     */
    public $password;
    /**
     *  角色
     *  @ORM\Column(type="string", length=128,nullable=true)
     */
    public $role;
    /**
     *  创建时间
     *  @ORM\Column(type="datetime", nullable=true)
     */
    public $createtime;
    /**
     *  修改时间
     *  @ORM\Column(type="datetime", nullable=true)
     */
    public $alerttime;
    /**
     *  备注
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $remark;
    /**
     *  状态
     *  @ORM\Column(type="integer",nullable=true)
     */
    public $state;

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getCreatetime()
    {
        return $this->createtime;
    }

    /**
     * @param mixed $createtime
     */
    public function setCreatetime($createtime)
    {
        $this->createtime = $createtime;
    }

    /**
     * @return mixed
     */
    public function getAlerttime()
    {
        return $this->alerttime;
    }

    /**
     * @param mixed $alerttime
     */
    public function setAlerttime($alerttime)
    {
        $this->alerttime = $alerttime;
    }

    /**
     * @return mixed
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param mixed $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }



}

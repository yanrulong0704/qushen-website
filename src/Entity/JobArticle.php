<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 14-8-12
 * Time: 下午8:21
 */
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * 文章表管理
 * @ORM\Entity
 * @ORM\Table(name="job_article")
 */
class JobArticle {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer $id
     */
    public $id;
    /**
     *  管理员id
     *  @ORM\Column(type="string", length=127,nullable=true)
     */
    public $agentid;
    /**
     *  文章分类
     *  @ORM\Column(type="string", length=127,nullable=true)
     */
    public $cid;
    /**
     *  文章名称
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $title;
    /**
     *  文章简介
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $intro;
    /**
     *  文章来源
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $cfrom;
    /**
     *  文章作者
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $author;
    /**
     *  缩略图
     *  @ORM\Column(type="string", length=512,nullable=true)
     */
    public $img;
    /**
     *  阅读次数
     *  @ORM\Column(type="string", length=127, nullable=true)
     */
    public $views;
    /**
     *  排序
     *  @ORM\Column(type="string", length=127, nullable=true)
     */
    public $orders;
    /**
     *  是否显示  0：不显示 1：显示
     *  @ORM\Column(type="integer", nullable=true)
     */
    public $isshow;
    /**
     *  文字内容
     *  @ORM\Column(type="text",nullable=true)
     */
    public $content;
    /**
     *  添加时间
     *  @ORM\Column(type="datetime",nullable=true)
     */
    public $addtime;

    /**
     * @return mixed
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * @param mixed $intro
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;
    }

    /**
     * @return mixed
     */
    public function getAgentid()
    {
        return $this->agentid;
    }

    /**
     * @param mixed $agentid
     */
    public function setAgentid($agentid)
    {
        $this->agentid = $agentid;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * @param mixed $cid
     */
    public function setCid($cid)
    {
        $this->cid = $cid;
    }


    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCfrom()
    {
        return $this->cfrom;
    }

    /**
     * @param mixed $cfrom
     */
    public function setCfrom($cfrom)
    {
        $this->cfrom = $cfrom;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return mixed
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param mixed $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    /**
     * @return mixed
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param mixed $orders
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
    }

    /**
     * @return mixed
     */
    public function getIsshow()
    {
        return $this->isshow;
    }

    /**
     * @param mixed $isshow
     */
    public function setIsshow($isshow)
    {
        $this->isshow = $isshow;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getAddtime()
    {
        return $this->addtime;
    }

    /**
     * @param mixed $addtime
     */
    public function setAddtime($addtime)
    {
        $this->addtime = $addtime;
    }

}
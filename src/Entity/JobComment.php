<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 14-8-12
 * Time: 下午8:21
 */
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * 评论表
 * @ORM\Entity
 * @ORM\Table(name="job_comment")
 */
class JobComment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    public $id;
    /**
     *  用户名
     *  @ORM\Column(type="integer",nullable=true)
     */
    public $userid;
    /**
     *  洽谈名称
     *  @ORM\Column(type="string", length=128,nullable=true)
     */
    public $title;
    /**
     *  创建时间
     *  @ORM\Column(type="datetime", nullable=true)
     */
    public $addtime;
    /**
     *  洽谈内容
     *  @ORM\Column(type="text", nullable=true)
     */
    public $content;
    /**
     *  状态
     *  @ORM\Column(type="integer",nullable=true)
     */
    public $state;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param mixed $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getAddtime()
    {
        return $this->addtime;
    }

    /**
     * @param mixed $addtime
     */
    public function setAddtime($addtime)
    {
        $this->addtime = $addtime;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }


}

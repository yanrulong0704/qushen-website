<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 14-8-12
 * Time: 下午8:21
 */
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * 公司信息
 * @ORM\Entity
 * @ORM\Table(name="company_info")
 */
class CompanyInfo {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    public $id;
    /**
     *  语言类型
     *  @ORM\Column(type="string", length=16,nullable=true)
     */
    public $language;
    /**
     *  公司名称
     *  @ORM\Column(type="string", length=127,nullable=true)
     */
    public $title;
    /**
     *  公司简介
     *  @ORM\Column(type="text",nullable=true)
     */
    public $info;
    /**
     *  联系电话
     *  @ORM\Column(type="string", length=127,nullable=true)
     */
    public $phone;
    /**
     *  联系电话2
     *  @ORM\Column(type="string", length=127,nullable=true)
     */
    public $phone_2;
    /**
     *  联系电话3
     *  @ORM\Column(type="string", length=127,nullable=true)
     */
    public $phone_3;
    /**
     *  电子邮箱
     *  @ORM\Column(type="string", length=127,nullable=true)
     */
    public $email;
    /**
     *  公司地址
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $address;

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getPhone2()
    {
        return $this->phone_2;
    }

    /**
     * @param mixed $phone_2
     */
    public function setPhone2($phone_2)
    {
        $this->phone_2 = $phone_2;
    }

    /**
     * @return mixed
     */
    public function getPhone3()
    {
        return $this->phone_3;
    }

    /**
     * @param mixed $phone_3
     */
    public function setPhone3($phone_3)
    {
        $this->phone_3 = $phone_3;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 14-8-12
 * Time: 下午8:21
 */
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * 友情表管理
 * @ORM\Entity
 * @ORM\Table(name="job_link")
 */
class JobLink {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer $id
     */
    public $id;
    /**
     *  链接名称
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    public $title;
    /**
     *  链接图片
     *  @ORM\Column(type="text",nullable=true)
     */
    public $img;
    /**
     *  链接地址
     *  @ORM\Column(type="text",nullable=true)
     */
    public $link;
    /**
     *  链接状态 1:有效 0:无效
     *  @ORM\Column(type="integer",nullable=true)
     */
    public $state;
    /**
     *  链接描述
     *  @ORM\Column(type="text",nullable=true)
     */
    public $info;
    /**
     *  添加时间
     *  @ORM\Column(type="datetime",nullable=true)
     */
    public $addtime;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getAddtime()
    {
        return $this->addtime;
    }

    /**
     * @param mixed $addtime
     */
    public function setAddtime($addtime)
    {
        $this->addtime = $addtime;
    }


}